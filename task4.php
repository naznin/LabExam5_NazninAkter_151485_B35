<?php

class factorial_of_a_number
{
    protected $_number;

    public function __construct($number)
    {
        if (!is_int($number)) {
            throw new InvalidArgumentException('Not a number or missing argument');
        }

        $this->_number = $number;

    }

    private function factorial($n) {
        if ($n == 1)    return 1;
        return $n * $this->factorial($n - 1);
    }
    public function result() {
        return $this->factorial($this->_number);
    }
}

$newfactorial = New factorial_of_a_number(5);

echo $newfactorial->result();
?>