<?php

$firstDate = new DateTime("1981-11-03");
$lastDate = new DateTime("2013-09-04");
$diffDate=$firstDate->diff($lastDate);

echo $diffDate->y . ($diffDate->y > 1 ? " years" : " year") . ", "
    . $diffDate->m . ($diffDate->m > 1 ? " months" : " month") . ", "
    . $diffDate->d . ($diffDate->d >1 ? " days" : " day");

?>